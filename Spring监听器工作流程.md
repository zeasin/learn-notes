# Spring监听器工作流程
### 一、Spring监听器原理
[参考](https://blog.csdn.net/qq_41792853/article/details/124630007)

[Spring监听器---ApplicationListener](https://www.cnblogs.com/nijunyang/p/12339757.html)

[Spring 监听器](https://zhuanlan.zhihu.com/p/562613720)

[深入理解Spring事件监听机制](https://www.cnblogs.com/xfeiyun/p/15316265.html)

[深入理解：Spring监听器使用方法与监听器的底层原理](https://blog.csdn.net/weixin_48033662/article/details/126337805)

### 二、SpringBoot Redis监听器

[SpringBoot 中使用Redis Stream 实现消息监听](https://blog.csdn.net/hhl18730252820/article/details/114826366)

[redis stream消息队列](https://www.jianshu.com/p/b20e9a0854f2)

[Redis Stream消息队列](https://blog.csdn.net/hhl18730252820/article/details/122702264)

### 三、SpringBoot Kafka监听器
[SpringBoot 整合 Kafka 以及 @KafkaListener 注解的使用](https://blog.csdn.net/yuxiangdeming/article/details/125004280)