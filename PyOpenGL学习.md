# PyOpenGL学习

## 一、初探
### 1.1 环境安装
+ Python:3.11.3
+ PyOpenGL:3.16

`pip install PyOpenGL`:运行提示错误，原因好像是官网只提供了32位的编译包。

解决方案:下载第三方编译好的64位包重新安装，下载地址:[https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyopengl](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyopengl)

+ 下载 `PyOpenGL‑3.1.6‑cp311‑cp311‑win_amd64.whl`
+ 重新安装`pip install --force-reinstall PyOpenGL‑3.1.6‑cp311‑cp311‑win_amd64.whl`
    
### 1.2 OpenGL绘制测试
[测试参考](https://blog.csdn.net/devcloud/article/details/128252499)

#### OpenGL绘制正方形
```
# -*- coding: utf-8 -*-
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
# 绘制图像函数
def display():
 # 清除屏幕及深度缓存
 glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
 # 设置红色
    glColor3f(1.0, 0.0, 0.0)
 # 开始绘制四边形
 glBegin(GL_QUADS)
 # 绘制四个顶点
    glVertex3f(-0.5, -0.5, 0.0)
    glVertex3f(0.5, -0.5, 0.0)
    glVertex3f(0.5, 0.5, 0.0)
    glVertex3f(-0.5, 0.5, 0.0)
 # 结束绘制四边形
 glEnd()
 # 清空缓冲区并将指令送往硬件执行
 glFlush()
# 主函数
if __name__ == "__main__":
 # 使用glut库初始化OpenGL
 glutInit()
 # 显示模式 GLUT_SINGLE无缓冲直接显示|GLUT_RGBA采用RGB(A非alpha)
 glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA)
 # 设置窗口位置大小
 glutInitWindowSize(400, 400)
 # 创建窗口
 glutCreateWindow("eastmount")
 # 调用display()函数绘制图像
 glutDisplayFunc(display)
 # 进入glut主循环
 glutMainLoop()
```

#### OpenGL绘制水壶
```
# -*- coding: utf-8 -*-
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
# 绘制图像函数
def drawFunc():
 # 清除屏幕及深度缓存
 glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
 # 设置绕轴旋转(角度,x,y,z)
 glRotatef(0.1, 5, 5, 0)
 # 绘制实心茶壶
 # glutSolidTeapot(0.5)
 # 绘制线框茶壶
 glutWireTeapot(0.5)
 # 刷新显示图像
 glFlush()
# 主函数
if __name__ == "__main__":
 # 使用glut库初始化OpenGL
 glutInit()
 # 显示模式 GLUT_SINGLE无缓冲直接显示|GLUT_RGBA采用RGB(A非alpha)
 glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA)
 # 设置窗口位置及大小
 glutInitWindowPosition(0, 0)
 glutInitWindowSize(400, 400)
 # 创建窗口
 glutCreateWindow("CSDN Eastmount")
 # 调用display()函数绘制图像
 glutDisplayFunc(drawFunc)
 # 设置全局的回调函数
 # 当没有窗口事件到达时,GLUT程序功能可以执行后台处理任务或连续动画
 glutIdleFunc(drawFunc)
 # 进入glut主循环
 glutMainLoop()
```
#### OpenGL绘制多个图形
```
# -*- coding: utf-8 -*-
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
# 绘制图像函数
def display():
 # 清除屏幕及深度缓存
 glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
 # 绘制线段
 glBegin(GL_LINES)
    glVertex2f(-1.0, 0.0) # 左下角顶点
    glVertex2f(1.0, 0.0) # 右下角顶点
    glVertex2f(0.0, 1.0) # 右上角顶点
    glVertex2f(0.0, -1.0) # 左上角顶点
 glEnd()
 # 绘制顶点
 glPointSize(10.0)
 glBegin(GL_POINTS)
    glColor3f(1.0, 0.0, 0.0) # 红色
    glVertex2f(0.3, 0.3)
    glColor3f(0.0, 1.0, 0.0) # 绿色
    glVertex2f(0.5, 0.6)
    glColor3f(0.0, 0.0, 1.0) # 蓝色
    glVertex2f(0.9, 0.9)
 glEnd()
 # 绘制四边形
    glColor3f(1.0, 1.0, 0)
 glBegin(GL_QUADS)
    glVertex2f(-0.2, 0.2)
    glVertex2f(-0.2, 0.5)
    glVertex2f(-0.5, 0.5)
    glVertex2f(-0.5, 0.2)
 glEnd()
 # 绘制多边形
    glColor3f(0.0, 1.0, 1.0)
 glPolygonMode(GL_FRONT, GL_LINE)
 glPolygonMode(GL_BACK, GL_FILL)
 glBegin(GL_POLYGON)
    glVertex2f(-0.5, -0.1)
    glVertex2f(-0.8, -0.3)
    glVertex2f(-0.8, -0.6)
    glVertex2f(-0.5, -0.8)
    glVertex2f(-0.2, -0.6)
    glVertex2f(-0.2, -0.3)
 glEnd()
 # 绘制三角形
    glColor3f(1.0, 1.0, 1.0)
 glPolygonMode(GL_FRONT, GL_FILL)
 glPolygonMode(GL_BACK, GL_LINE)
 glBegin(GL_TRIANGLES)
    glVertex2f(0.5, -0.5)
    glVertex2f(0.3, -0.3)
    glVertex2f(0.2, -0.6)
 # 结束绘制四边形
 glEnd()
 # 清空缓冲区并将指令送往硬件执行
 glFlush()
# 主函数
if __name__ == "__main__":
 # 使用glut库初始化OpenGL
 glutInit()
 # 显示模式 GLUT_SINGLE无缓冲直接显示|GLUT_RGBA采用RGB(A非alpha)
 glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA)
 # 设置窗口位置及大小
 glutInitWindowSize(400, 400)
 glutInitWindowPosition(500, 300)
 # 创建窗口
 glutCreateWindow("CSDN Eastmount")
 # 调用display()函数绘制图像
 glutDisplayFunc(display)
 # 进入glut主循环
 glutMainLoop()
```

#### OpenGL绘制时钟
```
# -*- coding: utf-8 -*-
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math
import time
h = 0
m = 0
s = 0
# 绘制图像函数
def Draw():
    PI = 3.1415926
    R = 0.5
    TR = R - 0.05
    glClear(GL_COLOR_BUFFER_BIT)
    glLineWidth(5)
    glBegin(GL_LINE_LOOP)
    for i in range(100):
        glVertex2f(R * math.cos(2 * PI / 100 * i), R * math.sin(2 * PI / 100 * i))
    glEnd()
    glLineWidth(2)
    for i in range(100):
        glBegin(GL_LINES)
        glVertex2f(TR * math.sin(2 * PI / 12 * i), TR * math.cos(2 * PI / 12 * i))
        glVertex2f(R * math.sin(2 * PI / 12 * i), R * math.cos(2 * PI / 12 * i))
        glEnd()
    glLineWidth(1)
    h_Length = 0.2
    m_Length = 0.3
    s_Length = 0.4
    count = 60.0
    s_Angle = s / count
    count *= 60
    m_Angle = (m * 60 + s) / count
    count *= 12
    h_Angle = (h * 60 * 60 + m * 60 + s) / count
    glLineWidth(1)
    glBegin(GL_LINES)
    glVertex2f(0.0, 0.0)
    glVertex2f(s_Length * math.sin(2 * PI * s_Angle), s_Length * math.cos(2 * PI * s_Angle))
    glEnd()
    glLineWidth(5)
    glBegin(GL_LINES)
    glVertex2f(0.0, 0.0)
    glVertex2f(h_Length * math.sin(2 * PI * h_Angle), h_Length * math.cos(2 * PI * h_Angle))
    glEnd()
    glLineWidth(3)
    glBegin(GL_LINES)
    glVertex2f(0.0, 0.0)
    glVertex2f(m_Length * math.sin(2 * PI * m_Angle), m_Length * math.cos(2 * PI * m_Angle))
    glEnd()
    glLineWidth(1)
    glBegin(GL_POLYGON)
    for i in range(100):
        glVertex2f(0.03 * math.cos(2 * PI / 100 * i), 0.03 * math.sin(2 * PI / 100 * i));
    glEnd()
    glFlush()
# 更新时间函数
def Update():
    global h, m, s
    t = time.localtime(time.time())
    h = int(time.strftime('%H', t))
    m = int(time.strftime('%M', t))
    s = int(time.strftime('%S', t))
    glutPostRedisplay()
# 主函数
if __name__ == "__main__":
 glutInit()
 glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA)
 glutInitWindowSize(400, 400)
 glutCreateWindow("My clock")
 glutDisplayFunc(Draw)
 glutIdleFunc(Update)
 glutMainLoop() 
```

## 二、OpenGL绘图代码及原理详解

### 2.1 核心函数

上述代码中，以glut开头的函数都是GLUT工具包所提供的函数。

| 函数	 |  描述	 |
| ------ | ------ |
| glutInit() | 对GLUT进行初始化，该函数必须在其它的GLUT使用之前调用一次。其格式比较死板，一般glutInit()直接调用即可。 | 
| glutInitDisplayMode() | 设置显示方式，其中GLUT_RGB表示使用RGB颜色，与之对应的是GLUT_INDEX（表示使用索引颜色）；GLUT_SINGLE表示使用单缓冲，与之对应的是GLUT_DOUBLE（表示使用双缓冲）。更多参数请读者阅读官方网站或Google。 | 
| glutInitWindowPosition() | 设置窗口在屏幕中的位置。 | 
| glutInitWindowSize() | 设置窗口的大小，两个参数表示长度和宽度。 | 
| glutCreateWindow() | 根据当前设置的信息创建窗口，参数将作为窗口的标题。需要注意的是，当窗口被创建后，并不是立即显示到屏幕上，需要调用glutMainLoop()才能看到窗口。 | 
| glutDisplayFunc() | 设置一个函数，当需要进行画图时，这个函数就会被调用，通常用来调用绘制图形函数。 | 
| glutMainLoop() | 进行一个消息循环，大家需要知道这个函数可以显示窗口，并且等待窗口关闭后才会返回。 | 
| glClear() | 清除，其中参数GL_COLOR_BUFFER_BIT表示清除颜色，GL_DEPTH_BUFFER_BIT表示清除深度。 | 
| glRectf() | 画一个矩形，四个参数分别表示位于对角线上的两个点的横、纵坐标。 | 
| glFlush() | 刷新显示图像，保证前面的OpenGL命令立即执行，而不是让它们在缓冲区中等待。 | 

以gl开头的函数都是OpenGL的标准函数。

OpenGL要求指定顶点的命令（glVertex2f）必须包含在glBegin()函数和glEnd()函数之间执行。

### 2.2 绘制顶点

顶点（vertex）是 OpengGL 中非常重要的概念，描述线段、多边形都离不开顶点。它们都是以glVertex开头，后面跟一个数字和1~2个字母，比如：

+ glVertex2d
+ glVertex2f
+ glVertex3f
+ glVertex3fv

数字表示参数的个数，2表示有2个参数（xy坐标），3表示三个（xyz坐标），4表示四个（齐次坐标 w）。字母表示参数的类型，s表示16位整数（OpenGL中将这个类型定义为GLshort），i表示32位整数（OpenGL中将这个类型定义为GLint和GLsizei），f表示32为浮点数（OpenGL中将这个类型定义为GLfloat和GLclampf），d表示64位浮点数（OpenGL中将这个类型定义为GLdouble和GLclampd）。例如：

+ glVertex2i(1, 3)
+ glVertex2f(1.0, 3.0)
+ glVertex3f(1.0, 3.0, 1.0)
+ glVertex4f(1.0, 3.0, 0.0, 1.0)

注意，OpenGL中很多函数都采用这种形式命名。

### 2.3 设置颜色

在OpenGL中，设置颜色函数以glColor开头，后面跟着参数个数和参数类型。参数可以是0到255之间的无符号整数，也可以是0到1之间的浮点数。三个参数分别表示RGB分量，第四个参数表示透明度（其实叫不透明度更恰当）。以下最常用的两个设置颜色的方法：

+ glColor3f(1.0，0.0，0.0) #红色
+ glColor3f(0.0，1.0，0.0) #绿色
+ glColor3f(0.0，0.0，1.0) #蓝色
+ glColor3f(1.0，1.0，1.0) #白色
+ glColor4f(0.0，1.0，0.0，0.0) #红色且不透明度
+ glColor3ub(255, 0, 0) #红色

注意，OpenGL是使用状态机模式，颜色是一个状态变量，设置颜色就是改变这个状态变量并一直生效，直到再次调用设置颜色的函数。除了颜色，OpenGL 还有很多的状态变量或模式。

### 2.4 绘制基本图形

前面我们介绍了各种图像，下表展示了常见的图像元件。

+ GL_POINTS：绘制顶点
+ GL_LINES：绘制线段
+ GL_LINE_STRIP：绘制连续线段
+ GL_LINE_LOOP：绘制闭合的线段
+ GL_POLYGON：绘制多边形
+ GL_TRIANGLES：绘制三角形
+ GL_TRIANGLE_STRIP：绘制连续三角形
+ GL_TRIANGLE_FAN：绘制多个三角形组成的扇形
+ GL_QUADS：绘制四边形
+ GL_QUAD_STRIP：绘制连续四边形

