# Python随用随学笔记
## 一、前提
### 1、环境安装

windows直接下载安装即可。版本3.11.3

### 2、Python基础学习

[官方指导文档](https://docs.python-guide.org/)
[官方中文文档](https://www.bookstack.cn/read/python-3.11.0-zh/189eceb25d7215c8.md)


[python库离线安装](https://blog.csdn.net/qq_38984928/article/details/127950192)

[python学习教程](http://c.biancheng.net/python/)

[python库下载地址](https://pypi.org/)

[python中文学习](https://docs.pythontab.com/)

#### 2.1基础语法


#### 2.2 pip

+ 查看已安装的库`pip list`
+ 卸载已安装的库`pip uninstall pytesseract -y`

##### Python库离线安装
[Python库离线下载地址](https://pypi.org/)

+ whl

    whl 安装命令如下，进入存放下载离线包的目录，在目录里输入cmd  进入命令窗口，再输入安装命令 回车

    `pip install pytesseract-0.3.10-py3-none-any.whl`
+ tar

    tar 的安装命令如下：进入存放下载离线包的目录，在目录里输入cmd  进入命令窗口，再输入安装命令 回车

    `python setup.py install`


##### pytesseract离线安装
+ 前提：packaging

##### opencv-python安装
+ 前提：numpy

##### matplotlib按照
前提：
+ contourpy
+ cycler
+ fonttools
+ kiwisolver
+ pyparsing
+ python-dateutil:依赖（six）



### 3、Python常用库
#### 基础
+ Requests：基于 urllib的开源 HTTP 库
+ urllib：python标准库，读取网页工具库
+ BeautifulSoup：html读取工具库
+ sqlite3：sql关系型小型文件数据库，python标准款
+ tinydb：nosql小型文件数据库，[参考](https://blog.csdn.net/m0_64036070/article/details/127164627)
+ tinui：基于tk画布的现代UI


#### AI相关
+ Numpy：NumPy是使用Python进行科学计算的基础软件包.[中文文档](https://www.numpy.org.cn/)
+ Matplotlib：图像处理库，生成图像 [中文文档](https://www.matplotlib.org.cn/)
+ opencv-python：图像处理库，处理图像数据 

    [官方英文文档](https://docs.opencv.org/) 

    [OpenCV-Python (官方)中文教程(部分一)](https://blog.csdn.net/Thomson617/article/details/103956799)

    [OpenCV-Python (官方)中文教程(部分二)](https://blog.csdn.net/Thomson617/article/details/103961274)

    [OpenCV-Python (官方)中文教程(部分三)](https://blog.csdn.net/Thomson617/article/details/103987952)

    [OpenCV-Python (官方)中文教程(部分四)](https://blog.csdn.net/Thomson617/article/details/103992899)

+ pytesseract：tesseract-ocr python封装库
+ Pillow：图像处理库
+ Natural language toolkit (NLTK)：最受欢迎的自然语言处理库(NLP)，它是用Python编写的，而且背后有非常强大的社区支持。
+ Pandas：一个强大的分析结构化数据的工具集；[中文文档](https://www.pypandas.cn/)


## 二、一些库的用法
### 2.1 opencv-python
opencv图像处理框架，开源的计算机视觉库，可以用于各种图像处理和计算机视觉任务。[参考1](https://www.osgeo.cn/opencv-python/ch03-core/sec01-basic-ops.html)

OpenCV的使用范围有哪些?

计算机视觉：OpenCV可用于计算机视觉任务，如目标检测、图像分类、人脸识别、姿态估计、运动估计、光流估计等。

视频分析：OpenCV可用于视频分析任务，如视频跟踪、行为识别、动作识别等。

机器学习：OpenCV集成了各种机器学习算法，如支持向量机、随机森林、朴素贝叶斯等，可以用于图像分类、目标检测等任务。

医学影像处理：OpenCV可以用于医学影像处理，如医学图像分割、医学图像配准、医学图像增强等。

自动驾驶：OpenCV可以用于自动驾驶，如车道检测、交通标志识别、障碍物检测等。

工业检测：OpenCV可以用于工业检测，如缺陷检测、产品分类、机器视觉等。

游戏开发：OpenCV可以用于游戏开发，如虚拟现实、增强现实等。

总之，OpenCV可用于各种图像处理和计算机视觉任务，它具有广泛的使用范围，可以应用于各种领域。



#### 图像的基本操作:
+ 访问和修改像素值
    `img = cv2.imread('/cvdata/messi5.jpg')`

+ 访问图像属性

+ 图像感兴趣区

+ 分割和合并图像通道
    ```
    b,g,r = cv2.split(img)
    img = cv2.merge((b,g,r))
    ```
+  为图像制作边框（填充）

#### 图像的算术运算
+ 图像加法

+ 图像混合

+ 按位运算

### 2.2 matplotlib
[画图示例](https://blog.csdn.net/qq_33267306/article/details/125587834)


### 2.3 NLP自然语言处理

[基础入门](https://blog.csdn.net/am1122/article/details/126852579)

[Python自然语言处理（入门讲解）](https://blog.csdn.net/zy1992As/article/details/129382127)

[[深度学习] 自然语言处理 --- NLP入门指南](https://zengwenqi.blog.csdn.net/article/details/103546648)


## 三、Tkinter
Python GUI标准库

[python之Tkinter使用详解](https://blog.csdn.net/qq_45664055/article/details/117625146)


[python --Tkinter详解](https://blog.csdn.net/weixin_44634704/article/details/122571178)

[TinUI:一套基于TK画布的现代UI](https://github.com/Smart-Space/tinui)

[tk中文文档](http://c.biancheng.net/tkinter/what-is-gui.html)



### 3.1 基本控件
#### 控件类型（基本）
| 控件类型	 | 控件名称	 | 控件作用 |
| ------ | ------ | ------ |
| Button	 | 按钮	 | 点击按钮时触发/执行一些事件（函数） |
| Canvas	 | 画布	 | 提供绘制图，比如直线、矩形、多边形等 |
| Checkbutton	 | 复选框	 | 多项选择按钮，用于在程序中提供多项选择框 |
| Entry	 | 文本框输入框	 | 用于接收单行文本输入 |
| Frame	 | 框架（容器）控件	 | 定义一个窗体（根窗口也是一个窗体），用于承载其他控件，即作为其他控件的容器 |
| Lable	 | 标签控件	 | 用于显示单行文本或者图片 |
| LableFrame	 | 容器控件	 | 一个简单的容器控件，常用于复杂的窗口布局。 |
| Listbox	 | 列表框控件 | 	以列表的形式显示文本 |
| Menu	 | 菜单控件	 | 菜单组件（下拉菜单和弹出菜单） |
| Menubutton	 | 菜单按钮控件	 | 用于显示菜单项 |
| Message	 | 信息控件	 | 用于显示多行不可编辑的文本，与 Label控件类似，增加了自动分行的功能 |
| messageBox	 | 消息框控件	 | 定义与用户交互的消息对话框 |
| OptionMenu	 |  选项菜单	 | 下拉菜单 |
| PanedWindow	 |  窗口布局管理组件 | 	为组件提供一个框架，允许用户自己划分窗口空间 |
| Radiobutton	 | 单选框	 | 单项选择按钮，只允许从多个选项中选择一项 |
| Scale	 | 进度条控件	 | 定义一个线性“滑块”用来控制范围，可以设定起始值和结束值，并显示当前位置的精确值 |
| Spinbox	 | 高级输入框	 | Entry 控件的升级版，可以通过该组件的上、下箭头选择不同的值 |
| Scrollbar	 | 滚动条	 | 默认垂直方向，鼠标拖动改变数值，可以和 Text、Listbox、Canvas等控件配合使用 |
| Text	 | 多行文本框	 | 接收或输出多行文本内容 |
| Toplevel	 | 子窗口 | 	在创建一个独立于主窗口之外的子窗口，位于主窗口的上一层，可作为其他控件的容器 |


#### 控件基本属性（通用）

| 属性名称	 | 说明 |
| ------ | ------ |
| anchor |	定义控件或者文字信息在窗口内的位置 |
| bg	 | bg 是 background 的缩写，用来定义控件的背景颜色，参数值可以颜色的十六进制数，或者颜色英文单词 |
| bitmap |	定义显示在控件内的位图文件 |
| borderwidth |	定于控件的边框宽度，单位是像素 |
| command |	该参数用于执行事件函数，比如单击按钮时执行特定的动作，可将执行用户自定义的函数 |
| cursor |	当鼠标指针移动到控件上时，定义鼠标指针的类型，字符换格式，参数值有 crosshair（十字光标）watch（待加载圆圈）plus（加号）arrow（箭头）等 |
| font |	若控件支持设置标题文字，就可以使用此属性来定义，它是一个数组格式的参数 (字体,大小，字体样式) |
| fg |	fg 是 foreground 的缩写，用来定义控件的前景色，也就是字体的颜色 |
| height |	该参数值用来设置控件的高度，文本控件以字符的数目为高度（px），其他控件则以像素为单位 |
| image |	定义显示在控件内的图片文件 |
| justify |	定义多行文字的排列方式，此属性可以是 LEFT/CENTER/RIGHT |
| padx/pady |	定义控件内的文字或者图片与控件边框之间的水平/垂直距离 |
| relief |	定义控件的边框样式，参数值为FLAT（平的）/RAISED（凸起的）/SUNKEN（凹陷的）/GROOVE（沟槽桩边缘）/RIDGE（脊状边缘） |
| text |	定义控件的标题文字 |
| state |	控制控件是否处于可用状态，参数值默认为 NORMAL/DISABLED，默认为 NORMAL（正常的） |
| width |	用于设置控件的宽度，使用方法与 height 相同 |


### 3.2 控件详解

#### Label

#### Button

#### Canves

#### Treeview
[Treeview基本用法](https://www.cnblogs.com/rainbow-tan/p/14125768.html)


#### PanedWindow

#### messagebox

#### LableFrame

#### Spinbox








## 四、Scrapy框架
scrapy是一个为了爬取网站数据，提取结构性数据而编写的应用框架。可应用在包括数据挖掘，信息处理或存储历史数据等一系列的程序中。其是一个半成品，能帮用户实现专业网络爬虫。

[scrapy简介](https://blog.csdn.net/langezuibang/article/details/115530491)


## 五、应用实例
### 1、ocr识别数据并结构化处理
应用说明：tesseract-ocr识别的结果使用python UI展示出来，并结构化处理保存到数据库。

[tesseract+opencv获取指定位置内容](https://blog.csdn.net/weixin_52051554/article/details/126087238)

[aircv+Pillow+Tesseract识别指定位置内容](https://www.zhangshengrong.com/p/QrXejmOk1d/)


**涉及到的技术记录如下：**

+ GUI图形界面：Tkinter（python标准库）；
+ subprocess：运行windows-cmd并获取输出；见https://blog.csdn.net/qq_27371025/article/details/120636383
+ tesseract-ocr：ocr识别（独立应用）
+ pytesseract：
+ aircv：用于识别模板再原始图的位置坐标
+ Pillow：图片处理库，利用Pillow库按图片标注的坐标裁剪图片，裁剪的图片OCR识别出来就是需要的数据，从而使数据可以结构化了。
+ opencv-python

#### 1.1 实现一

**代码**
```
from tkinter import *
from PIL import Image
import subprocess

"""
1、使用Pillow裁剪图片
2、使用tesseract识别裁剪图片内容
3、使用tk展示
"""
im = Image.open("D:\\QProject\\11.png")
box =(790,140,920,230)
im_crop = im.crop(box)
im_crop.show()
im_crop.save('11_p1.png')

cmd = 'tesseract 11_p1.png stdout -l chi_sim'

p = subprocess.Popen(
    cmd,
    shell=True,
    stdout=subprocess.PIPE,
    stderr=subprocess.STDOUT,
    encoding='utf-8'
)

# 输出stdout
res = p.communicate()[0]
res1 = res.split("\n")
print(res1)

root = TK()
# root.geometry('240x240')
lb = Label(root,text=res1[0])
lb.pack()
root.mainloop()
```

#### 1.2 实现二
+ pytesseract
+ Pillow

[使用python的opencv和tesseract库来识别图片中指定区域的中文](https://blog.csdn.net/huzhenwei/article/details/83508524)


```
from tkinter import *
import pytesseract
from PIL import Image

def ocr(path,lang):
    im = Image.open(path)
    text = pytesseract.image_to_string(im, lang=lang)
    print(text)

    box = (790, 140, 920, 230)
    im_crop = im.crop(box)
    # im_crop.show()
    # im_crop.save("11_p1.png")
    res = pytesseract.image_to_string(im_crop, lang=lang)

    root = TK()
    root.getometry('240x240')
    lb = Label(root, text=res)
    lb.pack()
    root.mainloop()

if __name__ == '__main__'
    ocr(r"D:\QProject\qq11.png",'chi_sim')

```

### 2、ocr识别网店流量数据并保存结构化数据
##### 2.1 流量截图

##### 2.2 利用cv2展示图片并记录鼠标点击位置，保存到模板
[cv2保存图片上的坐标参考](https://blog.csdn.net/weixin_43789195/article/details/105843771)

##### 2.3 根据步骤2保存的模板数据，用pytesseract+cv2识别按模板坐标裁剪的图片内容

##### 2.4 保存结构化之后的数据

