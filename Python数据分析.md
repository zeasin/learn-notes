# 大数据应用之Python

## 基础库
+ Numpy：多为数组计算
+ Pandas：数据分析
+ Matplotlib：图表（报表）绘制

## 案例一：热卖地区统计
#### 实现
+ 从mysql订单数据库提取出订单省份数据；(sqlalchemy、pymysql)
+ 利用pandas分组、排序；(pandas)
+ 生成词云；(wordcloud、pillow、numpy)
+ 展示结果；(matplotlib、tkinter)

#### 完整代码
+ 
```
import pandas as pd
from sqlalchemy import create_engine
from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from wordcloud import WordCloud
from tkinter import Tk

areas = []
# 从mysql数据库中读取数据
conn = create_engine("mysql+pymysql://{0}:{1}@{2}/{3}".format('root','123456','localhost:3306','db_name'),isolation_level='AUTOCOMMIT').connect()

sql = '''
        select province,city from t_order_area;
      '''


# 加载mysql数据并转换成df
df = pd.read_sql_query(sql, conn)
# 分组、排序
gdf = df.groupby(by=['province']).cout().sort_values(by='city',ascending=False)

# 组合排序后的province到areas
for tup in gdf.itertuples():
    areas.append(tup[0])

# 生成词云
img = Image.open(r'.\123.png')
img_array = np.array(img) # 将图片转化为数组

wc = WordCloud(
    background_color='#F8F8FF',
    mask=img_array,
    font_path=r"C:\Windows\Fonts\STXINGKA.TTF"
)
wc.generate_from_text(' '.join(areas))

# 绘制图片
fig = plt.figure(1)
plt.imshow(wc)
plt.axis('off') # 不显示坐标轴
# plt.show()

# tk显示
root = TK()
canvas1 = FigureCanvasTkAgg(fit,master=root)
canvas1.draw()
canvas1.get_tk_widget().grid()
root.mainloop()
```
