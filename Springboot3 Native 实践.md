# Springboot3 Native 实践

**系统环境：MacOS10.15**

## 1、安装设置GraalVM
##### 1.1 下载GraalVM
下载地址：https://github.com/graalvm/graalvm-ce-builds/releases
##### 1.2 配置环境变量
```
export JAVA_HOME=/Library/Java/JavaVirtualMachines/graalvm-ce-java17-22.3.2/Contents/Home
export GRAALVM_HOME=$JAVA_HOME
export MAVEN_HOME=/Users/richie/Downloads/apache-maven-3.9.1
export PATH=$PATH:$JAVA_HOME/bin:$GRAALVM_HOME/bin:$MAVEN_HOME/bin:
```
##### 1.3 下载安装native-image
+ 下载native-image：https://github.com/graalvm/graalvm-ce-builds/releases
+ 安装native-image
	`gu install -L native-image...`

##### 1.4 测试
+ 新建HelloWorld
+ javac HelloWorld.java
+ native-image HelloWorld

## 2、创建springboot3项目