# 学习笔记

## Getting started

+ 笔记存储：gitlab.com/zeasin/learn-notes
+ 笔记工具：gitlab.com webide
+ 画图存储：gitlab.com/zeasin/drawiofile
+ 画图工具：draw.io



create by：gitlab webide

create on：2023-3-27 20：00

## 最近学习任务

- [x] 熟练使用vue、auve、elementUI
- [x] 深入学习java8新特性
- [x] 注解与AOP结合实际应用
- [x] 使用springboot内置web服务器Undertow
- [x] 掌握kafka（基本使用KafkaTemplate、Kafka Tool）
- [x] 掌握Redis基本使用（安装，缓存、消息队列，RedisInsight）
- [ ] 持续学习JVM
    - [ ] Java SPI机制

- [ ] 持续学习Spring
    - [x] 理解并记录SpringBoot自动装配全过程
    - [ ] Spring SPI机制
    - [ ] Spring监听器工作流程
- [ ] Java9新特性-模块系统Jigsaw 
- [ ] 掌握mongodb  
- [ ] 学习Three.js基本概念
- [ ] 继续深入flowable应用
- [ ] Graalvm-Java云原生未来
`GraalVM Native Images是一个利用AOT(Ahead-of-Time)技术把java程序直接编译成可执行程序的编译工具，编译出来的程序在运行时不再依赖JRE，同时启动速度快，资源消耗低，这对传统java程序来说都是极大的优势。同时云原生应用来说，GraalVM Native Images编译生成的程序体积很小，非常适合云原生环境，目前由于传统java程序生成的镜像中需要包含一个体积很大的JRE或JDK而经常被人诟病。`
- [ ] Spring 6
    - [ ] AOT


- [ ] 图形引擎
    - [ ] Three.js
    - [ ] jMonkeyEngine
    - [ ] unity3D