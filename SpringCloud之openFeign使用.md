# SpringClound之OpenFeign
## 一、OpenFeign概述
openFeign是什么？
前面介绍过停止迭代的Feign，简单点来说：OpenFeign是springcloud在Feign的基础上支持了SpringMVC的注解，如@RequestMapping等等。OpenFeign的@FeignClient可以解析SpringMVC的@RequestMapping注解下的接口，并通过动态代理的方式产生实现类，实现类中做负载均衡并调用其他服务。


[参考](https://blog.csdn.net/agonie201218/article/details/121154769)



## 二、OpenFeign使用
[OpenFeign理解和使用](https://blog.csdn.net/m0_45016797/article/details/123633386)

[Spring Cloud OpenFeign - 远程调用](https://baijiahao.baidu.com/s?id=1737466192733578192&wfr=spider&for=pc)

[OpenFeign 调用第三方接口 绕过https认证](https://blog.csdn.net/m0_46583587/article/details/126655678)

