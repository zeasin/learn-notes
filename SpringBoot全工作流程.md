# SpringBoot全工作流程
[参考-未看完](https://www.cnblogs.com/theRhyme/p/how-does-springboot-start.html)
[参考-待看](https://www.cnblogs.com/theRhyme/p/11057233.html)
## 一、自动装配

[参考](https://blog.csdn.net/qq_27028647/article/details/129266436)

自动装配大致过程如下：

+ 获取到组件（例如spring-boot-starter-data-redis）META-INF文件夹下的spring.factories文件

+ spring.factories文件中列出需要注入IoC容器的类

+ 将实体类注入到IoC容器中进行使用

![img](https://img-blog.csdnimg.cn/img_convert/6df0a7dbafb988daec801e9700fcc71a.png)

**核心类**
+ AutoConfigurationImportSelector（invokeBeanFactoryPostProcessors）


## 二、SpringBoot启动流程

![img](https://img-blog.csdnimg.cn/f48383125dc04a35bfd05a9d01b950cb.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBARmx55Li2WA==,size_20,color_FFFFFF,t_70,g_se,x_16)

[参考1](https://www.cnblogs.com/deity-night/p/17146509.html)

[参考2](https://blog.csdn.net/weixin_44947701/article/details/124055713)

## 三、IOC容器装载（AbstractApplicationContext.refresh()核心十二步）

[参考](https://blog.csdn.net/weixin_45841729/article/details/123976379)

