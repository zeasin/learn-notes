# Java版本

## Java 8 新特性

+ 1、Lamdba表达式
`从匿名类到Lambda转换`
+ 2、函数式接口
`Java内置函数式接口Function、Consumer、Supplier、Predicate`
+ 3、方法引用和构造引用
    + 方法引用：使用操作符“：：”将方法名和（类或者对象）分割开来。
        `对象：：实例方法` `类：：实例方法` `类：：静态方法`
    + 构造器引用 `ClassName :: new`
+ 4、Stream API

+ 5、接口中的默认方法和静态方法

+ 6、新时间日期API

+ 7、OPtional

+ 8、接口的默认方法和静态方法
```
public interface MyInterface {
   default void myMethod() {
       // 默认方法的实现代码
   }
}
```
```
public interface MyInterface {
   static void myStaticMethod() {
       // 静态方法的实现代码
   }
}
```


## Java 9 新特性
+ 模块化系统
```
如果把 Java 8 比作单体应用，那么引入模块系统之后，从 Java 9 开始，Java 就华丽的转身为微服务。模块系统，项目代号 Jigsaw，最早于 2008 年 8 月提出(比 Martin Fowler 提出微服务还早 6 年)，2014 年跟随 Java 9 正式进入开发阶段，最终跟随 Java 9 发布于 2017 年 9 月。
```
```
引入模块系统之后，JDK 自身被划分为 94 个模块(参见图-2)。通过 Java 9 新增的 jlink 工具，开发者可以根据实际应用场景随意组合这些模块，去除不需要的模块，生成自定义 JRE，从而有效缩小 JRE 大小。得益于此，JRE 11 的大小仅为 JRE 8 的 53%，从 218.4 MB缩减为 116.3 MB，JRE 中广为诟病的巨型 jar 文件 rt.jar 也被移除。更小的 JRE 意味着更少的内存占用，这让 Java 对嵌入式应用开发变得更友好。
```

+ HTTP Client API
在java9及10被标记incubator的模块jdk.incubator.httpclient，在java11被标记为正式，改为java.net.http模块。
这是 Java 9 开始引入的一个处理 HTTP 请求的的孵化 HTTP Client API，该 API 支持同步和异步，而在 Java 11 中已经为正式可用状态，你可以在 java.net 包中找到这个 API。

来看一下 HTTP Client 的用法：
```
var request = HttpRequest.newBuilder()
    .uri(URI.create("https://javastack.cn"))
    .GET()
    .build();
var client = HttpClient.newHttpClient();
// 同步
HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
System.out.println(response.body());
// 异步
client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
    .thenApply(HttpResponse::body)
    .thenAccept(System.out::println);

```
+ Java 的 REPL 工具： jShell 命令
+ 语法改进：接口的私有方法
在 Java 9 中，接口更加的灵活和强大，连方法的访问权限修饰符都可以声明为 private 的了，此时方法将不会成为你对外暴露的 API的一部分
```
public static String staticFun() {
    privateFun();
    return "";
}

default String defaultFun() {
    privateFun();
    return "";
}

private static void privateFun() {
    System.out.println("我是私有方法~");
}
```

## Java 10 新特性
+ 局部变量类型推断`var`


## Java 11 新特性
+ InputStream 加强
InputStream 终于有了一个非常有用的方法：transferTo，可以用来将数据直接传输到 OutputStream，这是在处理原始数据流时非常常见的一种用法，如下示例。
```
var classLoader = ClassLoader.getSystemClassLoader();
var inputStream = classLoader.getResourceAsStream("javastack.txt");
var javastack = File.createTempFile("javastack2", "txt");
try (var outputStream = new FileOutputStream(javastack)) {
    inputStream.transferTo(outputStream);
}
```

## Java 14 新特性
+ 引入打包工具`jpackage`
使用场景是面向java桌面端程序打包. 可以让windows/mac 直接双击使用java程序,对系统里面有没有jdk/jre不做要求.

## Java 15 新特性
+ Sealed Classes（密封类）预览
+ Hidden Classes（隐藏类）
+ 文本块
```
String content = """
        {
            "upperSummary": null,\
            "sensitiveTypeList": null,
            "gmtModified": "2011-08-05\s10:50:09",
        }
         """;
System.out.println(content);
```
+ ZGC: 可扩展低延迟垃圾收集器（正式发布）

## Java 16 新特性
+ Record
以前我们定义类都是用class关键词，但从Java 16开始，我们将多一个关键词record，它也可以用来定义类。record关键词的引入，主要是为了提供一种更为简洁、紧凑的final类的定义方式。
`record range(int start, int end){}`