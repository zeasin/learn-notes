# 自定义注解翻译字典
### 使用场景
+ 列表性别字段展示名称

### 1、定义字段注解（要翻译的字段注解）
```
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumDict{
    String enumName();
    String dictField() default "";
    String suffix() default "Label"
}

```

### 2、定义切面注解
```
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumDictCovert{

}

```

### 3、切面处理
```
@Slf4j
@Aspect
@Component
public class EnumDictAspect{

    /**
     * 表示匹配带有自定义注解的方法 根据 DictCovert 存放包名设置
     */
    @Pointcut("@annotation(com.**.EnumDictCovert)")
    public void pointcut() {

    }

    /**
     * 环绕通知注解匹配，处理字典翻译
     */
    @Around("pointcut()")
    public Object aroundAdvice(ProceedingJoinPoint point) throws Throwable {
        //返回结果
        Object result = point.proceed();
        return EnumDictUtils.convertDict(result);
    }

}
```
### 4、字典的实现类
```
public class EnumDictUtils{
    public static Object convertDict(Object target){
        if(target instanceof List){
            List<Object> list = new ArrayList();
            for(Object obj : (List<Object>) tartget){
                list.add(parsedDict(obj))
            }
            return list;
        }else{
            Object obj = parsedDict(target);
            return obj;
        }
    }

    public static Object parsedDict(Object obj){
        Field[] fields = obj.getClass().getDeclaredFields();
        if(fields == null || fields.length == 0 ) return obj;
        try{
            ObjectMapper mapper = new ObjectMapper();
            Map map = mapper.readValue(mapper.writeValueAsString(obj),Map.class);
            for(Field field : fields){
                //判断字段是否有Dict注解
                if(Objects.nonNull(field.getAnnotation(EnumDict.class))){
                    EnumDict dict = field.getAnnotation(EnumDict.class);
                    String fieldName = field.getName();
                    Object fieldValue = map.get(fieldName);
                    //从字典中读取值并赋值给新字段，插入到map中
                    map.put(fieldName+ dict.suffix(),getDictText(dict,fieldValue));
                }
            }
            //将处理之后的map转换成Class对象，并返回出去
            return mapper.readValue(mapper.writeValueAsString(map),obj.getClass());

        }catch(Exception e){
            return obj;
        }
    }

    public static String getDictText(EnumDict dict,Object value){
        if(Objects.isNull(value)) return "";
        String val = value.toString();
        List<NameValueVo> dicts = new ArrayList<>();
        dicts.add(new NameValueVo(0,"female","女"));
        dicts.add(new NameValueVo(1,"male","男"));

        if(CollectionUtils.isEmpty(dicts)) return "";

        List<String> vals = Arrays.asList(val.split(","));

        List<String> texts = vals.stream()
                    .map(v -> dicts.strean()
                            .filter(t -> t.getIndex().toString().equals(v))
                            .findFirst()
                            .map(NameValueVo::getName)
                            .orElse(null))
                    .filter(StringUtils::hasText)
                    .collecct(Collectors.toList()    
                    );
        return texts.get(texts.size() - 1);
    }

}
```



