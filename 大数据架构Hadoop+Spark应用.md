# 大数据架构Hadoop+Spark应用
## 一、环境准备

### 1.1 概述
#### Hadoop
用来存储数据，主要用到hdfs。[Hadoop 之 HDFS 详解](https://blog.csdn.net/m0_67393039/article/details/126365617)

#### Spark
Spark是专为大规模数据处理而设计的快速通用的计算引擎。[详解](https://mikechen.cc/21992.html)


版本要求：
+ Hadoop:3.2.4 [https://archive.apache.org/dist/hadoop/common/hadoop-3.2.4/](https://archive.apache.org/dist/hadoop/common/hadoop-3.2.4/)
+ Spark:3.2.4 [https://www.apache.org/dyn/closer.lua/spark/spark-3.2.4/spark-3.2.4-bin-hadoop3.2.tgz](https://www.apache.org/dyn/closer.lua/spark/spark-3.2.4/spark-3.2.4-bin-hadoop3.2.tgz)
+ winutils-master:3.2.2 [https://github.com/cdarlint/winutils/tree/master/hadoop-3.2.2/bin](https://github.com/cdarlint/winutils/tree/master/hadoop-3.2.2/bin)
+ Scala:2.12
+ Python:3.11.3

### 1.2 安装配置

[windows环境配置](https://blog.csdn.net/weixin_41936572/article/details/126636382)

[hadoop安装参考1](https://www.jianshu.com/p/b55fa16430fe)

管网无法下载，使用下面的地址下载：

[hadoop-3.2.4.tar.gz](https://mirrors.tuna.tsinghua.edu.cn/apache/hadoop/common/hadoop-3.2.4/)

[spark-3.2.4-bin-hadoop3.2.tgz](https://mirrors.tuna.tsinghua.edu.cn/apache/spark/spark-3.2.4/)


### 1.3 测试
+ JDK：17
+ Python：3.11.3
+ Spark：3.4.0
+ Haddoop：3.3.5
+ Scala:2.12.17
+ 开发工具：Idea2022.3

#### Idea配置Scala开发环境
+ 安装Idea插件Scala

#### 项目构建Maven
```
<dependencies>
    <dependency>
        <groupId>org.scala-lang</groupId>
        <artifactId>scala-library</artifactId>
        <version>2.12.17</version>
    </dependency>
    <dependency>
        <groupId>org.apache.spark</groupId>
        <artifactId>spark-core_2.12</artifactId>
        <version>3.4.0</version>
    </dependency>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13.1</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```

#### 编写Scala代码
```
package org.example.scala

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object SparkTest {
  def main(args: Array[String]): Unit = {
    val conf: SparkConf = new SparkConf().setAppName("sc").setMaster("local[*]")
    val sc: SparkContext = new SparkContext(conf)
    sc.setLogLevel("WARN")


    val lines:RDD[String] = sc.textFile("data/t1.txt")
    val words:RDD[String] = lines.flatMap(_.split(","))
    val wordAndCount: RDD[(String,Int)] = words.map((_,1))
    val result: RDD[(String,Int)] = wordAndCount.reduceByKey(_+_)

//    result.foreach(println)
    println(result.collect().toBuffer)
    Thread.sleep(100 * 60)
    println("结束")
  }

}
```

#### 运行

Java17运行spark会报错，首先错误原因是：jdk17使用了模块化，导致一些spackage没有权限，需要在vm参数里配置：

```
--add-opens=java.base/java.lang=ALL-UNNAMED
--add-opens=java.base/java.lang.invoke=ALL-UNNAMED
--add-opens=java.base/java.lang.reflect=ALL-UNNAMED
--add-opens=java.base/java.io=ALL-UNNAMED
--add-opens=java.base/java.net=ALL-UNNAMED
--add-opens=java.base/java.nio=ALL-UNNAMED
--add-opens=java.base/java.util=ALL-UNNAMED
--add-opens=java.base/java.util.concurrent=ALL-UNNAMED
--add-opens=java.base/java.util.concurrent.atomic=ALL-UNNAMED
--add-opens=java.base/sun.nio.ch=ALL-UNNAMED
--add-opens=java.base/sun.nio.cs=ALL-UNNAMED
--add-opens=java.base/sun.security.action=ALL-UNNAMED
--add-opens=java.base/sun.util.calendar=ALL-UNNAMED
```

**注：不一定需要所有，根据你的错误添加，我这里加了其中的3条**

#### 运行结果
`ArrayBuffer((82,1), (城,1), (达,1), (深蓝色,15), (莆田市,2), (所在,1), (武穴市,1), (组团,2), (13,1), (福安市,1), (其,4), (购物,1), (8026,7), (1882778559753466768,6), (双,6), (福海,3), (贵州省,1), (年,2), (灰色,4), (顺,2), (北二路,1), (南路,2), (18941173129,2), (梅香,2), (阳春市,6), (vivo,2), (一,1), (连,2), ((,1), (9016,1), (单元,6), (山东省,2), (福建省,5), (东风,2), (广安,1), (件,48), (昆州,2), (城厢区,2), (花桥,2), (9270,2), (2xl,9), (抖,1), (张,5), (的,2), (李,2), (达州市,6), (\,3), (坪,1), (米利,2), (俊,2), (河东路,2), (后门,1), (n,3), (1882451820880953699,2), (尾,2), (昆明市,2), (深圳市,6), (瑞,3), (秀,2), (宁德市,1), (厂,1), (惠州市,1), (3336859910330166853,2), (溪,1), (浅蓝色,25), (6815,1), (406,1), (玲,8), (黄家,6), (8,1), (周,1), (晴,2), (幢,12), (白花,1), (楼,2), (黄冈市,1), (西山区,2), (街道,36), (幸运,2), (123,2), (宝安区,5), (阳江市,8), (姐,6), (浦,3), (香花,1), (熊,6), (湖北省,1), (陶,6), (专卖店,2), (温泉,2), (菜市场,2), (901,6), (旅顺口区,2), (从,1), (淘,16), (尤,2), (13076669688,1), (凤,4), (雅园,1), (3336771243235968637,2), (源,2), (大道,7), (金荣,1), (遵义市,1), (9043,1), (hn8026,44), (11,1), (华,1), (佛山市,1), (区,1), (卫生,2), (三星,6), (世纪,1), (花园,6), (洞,1), (000000,1), (兴,9), (17671048224,1), (金堂县,6), (15860943059,2), (公司,1), (淮,6), (新联,2), (丰,2), (中山市,1), (3344699955595122436,1), (小丽,6), (xl,10), (龙王,2), (3345140018325587225,1), (4039,2), (4,3), (龙岗区,3), (hn608,4), (西苑,2), (荔,1), (6,7), (13765242239,1), (18466171035,1), (2,3), (86,1), (莺歌,6), (沈,6), (和,1), (乐,1), (l,10), (江山,6), (13183941884,4), (地址,1), (留洋,1), (地区,1), (订单号,30), (塘,3), (7382,2), (13652389209,2), (逸,1), (旺,1), (世界,1), (府,6), (渝北区,6), (曾,1), (二楼,2), (,24), (维,1), (1882913486651018285,2), (【,7), (天下,1), (港口,1), (刘,2), (3336126841057672228,2), (社区,1), (15766454282,2), (徐,1), (18480103625,1), (文龙,3), (欣怡,1), (南安市,4), (坪地,3), (详细,1), (广东省,19), (15323739231,1), (江苏省,3), (黑色,8), (702,1), (招远市,2), (蒲江,6), (3335842441743936329,6), (订单,17), (金丰,1), (1882218072472706778,2), (肉,4), (重庆,6), (辽宁省,2), (仙,1), (去,1), (档,2), (浩,2), (小区,8), (东源县,1), (怡,1), (新居,6), (沛,1), (阳,1), (玛,2), (苑,2), (15018121756,2), (顺德区,1), (17284437205,1), (宝,7), (黄海,1), (黑,4), (恒康,1), (],19), (金,2), (收货人,1), (村,2), (设计,2), (美发,1), (一路,2), (昆山市,3), (8612,6), (编程,2), (欣,2), (惠东县,1), (成都市,6), (98,1), (广场,2), (1440,6), (不锈钢,2), (苏州市,3), (9,2), (盛,1), (手机号码,1), (工业区,2), (1,1), (圣湖,6), (泉州市,2), (太阳,1), (m,16), (10,9), (江,2), (15198833225,2), (新街,1), (s,10), (3336143978706388808,6), (】,7), (超市,4), (丽,2), (17896409944,6), (3336360265060151258,2), (18468561871,2), (园,2), (梁,8), (小青,1), (河源市,1), (211762,1), (50,2), (清,1), (佳,2), (广,1), (商品,29), (何,2), (有限公司,3), (3242,1), (居委会,1), (12,1), (一号,2), (海,1), (苏,2), (清远市,1), (3336858290452166853,2), (6991,1), (莉,1), (海塘,2), (街,5), (培训班,2), (3344186018439513114,1), (林,6), (9738,1), (龙台,8), (小学,2), (语,2), (栋,2), (18466249311,1), (南,2), (烟台市,4), (迎春,2), (坳,1), (云南省,2), (6918240533988906631,2), (妹,1), (威达,1), (万,4), (号,13), (街上,1), (c,1), (娟,6), (发财,2), (),1), (正安县,1), (14749442404,6), (特,2), (5,2), (玫瑰,2), (复古,2), (镇,20), (科技,2), (17806517136,1), (对面,5), (7363,4), (花城,1), (白色,1), (服饰,1), (四川省,20), (彭,1), (13350283318,6), (安岳县,8), (小姐,1), (kkrgbtn42,1), (7648,2), (英,2), (福,2), ([,19), (科,6), (港,2), (江城区,2), (大连市,2), (心,1), (14,1), (7,1), (新镇,2), (鞋业,2), (华亭,2), (重庆市,6), (青龙,4), (玉堂,6), (b1,1), (钢铁,1), (3,2), (蓝,2), (路,7), (-,30), (13635439373,6), (郑,1), (鹏,2), (珍,1), (友,1), (15960547510,2), (口,6), (大,6), (3336516291292705403,2), (清城区,1), (小,1), (鸿,1), (18466114772,1), (1882847425626750071,6), (a,1), (宣汉县,6), (梅,1), (资阳市,8), (春城,6), (敏,3), (x1,48), (洁,1), (便利店,8), (三,6), (伟,2), (王,5), (18364533482,2), (省,2), (17306180058,1))`



## 二、应用入门
### 2.1 Hadoop

[Hadoop原理与技术——hdfs命令行基本操作](https://blog.csdn.net/qq_47888212/article/details/128140358)

### 2.2 Spark
[文章1](https://blog.csdn.net/m0_64768308/article/details/126843535)

[！！！史上最全Spark常用算子总结 ！！！](https://blog.csdn.net/kiritobryant/article/details/128466040)

##### Spark基本概念
+ RDD：是弹性分布式数据集（Resilient Distributed Dataset）的简称，是分布式内存的一个抽象概念，提供了一种高度受限的共享内存模型。
+ DAG：是Directed Acyclic Graph（有向无环图）的简称，反映RDD之间的依赖关系。
+ Driver Program：控制程序，负责为Application构建DAG图。
+ Cluster Manager：集群资源管理中心，负责分配计算资源。
+ Worker Node：工作节点，负责完成具体计算。
+ Executor：是运行在工作节点（Worker Node）上的一个进程，负责运行Task，并为应用程序存储数据。
+ Application：用户编写的Spark应用程序，一个Application包含多个Job。
+ Job：作业，一个Job包含多个RDD及作用于相应RDD上的各种操作。
+ Stage：阶段，是作业的基本调度单位，一个作业会分为多组任务，每组任务被称为“阶段”。
+ Task：任务，运行在Executor上的工作单元，是Executor中的一个线程。

**总结：Application由多个Job组成，Job由多个Stage组成，Stage由多个Task组成。Stage是作业调度的基本单位。**

[spark参考1](https://blog.csdn.net/m0_64768308/article/details/126843535)

[Spark简述](https://blog.csdn.net/ran_hao/article/details/104960706)




#### 2.2.1 Spark基本

[Spark的算子的分类](https://blog.csdn.net/King_S_H/article/details/125231706)



#### 2.2.2 Spark MLlib



[文章1](https://www.cnblogs.com/feiyumo/p/8603249.html)

[【Spark NLP】第 1 章：入门](https://blog.csdn.net/sikh_0529/article/details/127561085)

[Spark NLP的一些基础方法](https://blog.csdn.net/dqz_nihao/article/details/118440548)

[Spark学习之路——9.Spark ML](https://blog.csdn.net/hehe_soft_engineer/article/details/104158238)

[Spark MLlib实现的中文文本分类](https://blog.csdn.net/helloxiaozhe/article/details/118072573)

[Spark MLlib 环境搭建超详细教程](https://www.jianshu.com/p/9f40fe1b6587)

[spark的学习（2）之计算最受欢迎美食种类然后画词云](https://blog.csdn.net/lihao1107156171/article/details/119926599)

[spark+jieba分词+wordclound生成斗破苍穹词云图](https://www.jianshu.com/p/7251ef4aa36d)

[最全中文停用词表整理（1893个）](https://blog.csdn.net/shijiebei2009/article/details/39696571)


## 三、案例实战
scala
```
package com.ecom.scala

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Test {
  def main(args: Array[String]): Unit = {
    val conf: SparkConf = new SparkConf().setAppName("sc").setMaster("local[*]")
    val sc: SparkContext = new SparkContext(conf)
    sc.setLogLevel("WARN")

    val lines:RDD[String] = sc.textFile("data/README.md")
    val words:RDD[String] = lines.flatMap(_.split(","))
    val wordAndCount: RDD[(String,Int)] = words.map((_,1))
    val result: RDD[(String,Int)] = wordAndCount.reduceByKey(_+_)

    result.foreach(println)
    println(result.collect().toBuffer)

  }

}
```

### 3.1 中文分词并统计词频并排序
+ 分词：HanLP
+ 核心思路：
  + 从hdfs读取文件进行分词；
  + 将分好的词过滤并组合成List；
  + 将整理好的分词List生成JavaPairRDD；
  + JavaPairRDD组合成[word,12]这种数据，并分组；
  + 将JavaPairRDD转换成JavaRDD并排序；
  + 取出结果并打印；


+ 依赖
```
<dependency>
  <groupId>org.scala-lang</groupId>
  <artifactId>scala-library</artifactId>
  <version>2.12.17</version>
</dependency>
<dependency>
  <groupId>org.apache.spark</groupId>
  <artifactId>spark-core_2.12</artifactId>
  <version>3.4.0</version>
</dependency>
<dependency>
  <groupId>com.hankcs</groupId>
  <artifactId>hanlp</artifactId>
  <version>portable-1.8.4</version>
</dependency>
```

#### Java实现代码

```
private static List<String> wordList = new ArrayList();
public static void main(String[] args){
  SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("scTest");
  JavaSparkContext sc = new JavaSparkContext(conf);
  JavaRDD<String> stringJavaRDD = sc.textFile("hdfs://127.0.0.1:9000/1.txt");
  //中文分词
  JavaRDD<List<Term>> map =  stringJavaRDD.map(w -> {return StandardTokenizer.segment(w);});

  //过滤分词结果，并重新组成List<String>
  map.foreach(l -> {
    l.forEach(w -> {
      if (w.word.length() > 1) wordList.add(w.word);
    });
  });

  //将List<String>转换成JavaPairRDD并分组
  JavaPairRDD<String,Integer> jr = sc.parallelize(wordList)
      .mapToPair(word -> {return new Tuple2<>(word,1);})
      .reduceByKey((x,y) -> (x + y));

  //将JavaPaidRDD转换成JavaRDD并按数值排序
  JavaRDD<Tuple2<String,Integer>> tjr = jr.map(w ->{return w;})
      .sortBy(S -> {return s._2;},false,2);
  
  //打印结果
  tjr.collect().forEach(System.out::println);
  //关闭Spark资源
  sc.stop();
}
```

#### Scala实现代码
```
val words: util.List[String] = new util.ArrayList[String]
def main(args: Array[String]): Unit = {
  val conf: SparkConf = new SparkConf().setMaster("local[*]").setAppName("sparkCoreTest")
  val sc = new SparkContext(conf)
  val rdd: RDD[String] = sc.textFile("data/t1.txt",2)
  val map: RDD[util.List[Term]] = rdd.map(w => StandardTokenizer.segment(w))

  //组合分词
  for (ls <- map){
    for (s: Term <- ls){
      if(s.word.length > 1) words.add(s.word)
    }
  }

  //词频统计排序
  val tuples = sc.paralletize(words)
    .map(w => new Tuple2[String,Integer](w, 1))
    .reduceByKey(_+_)
    .sortBy(_._2,false)
    .collect()

  //打印
  tuples.foreach(println)
}
```



