# Redis应用

## 一、Redis概览
[官方文档](https://redis.io/docs/data-types/sorted-sets/)
### 1.1 概览
官方定义:`数百万开发人员用作数据库、缓存、流引擎和消息代理的开源内存数据存储.`
redis是一个开源的、使用C语言编写的、支持网络交互的、可基于内存也可持久化的Key-Value数据库。

### 1.2 基本数据类型
[五个基本类型](https://blog.csdn.net/doomwatcher/article/details/120623082)
#### String
String是Redis最基本的类型，你可以理解成与Memcached一模一样的类型，一个key对应一个value。
String类型是二进制安全的。意味着Redis的string可以包含任何数据。比如jpg图片或者序列化的对象。
String类型是Redis最基本的数据类型，一个Redis中字符串value最多可以是512M

#### List
单键多值
Redis 列表是简单的字符串列表，按照插入顺序排序。你可以添加一个元素到列表的头部（左边）或者尾部（右边）。
它的底层实际是个双向链表，对两端的操作性能很高，通过索引下标的操作中间的节点性能会较差。
![img](https://img-blog.csdnimg.cn/d50779cc32c5471a90423259b4289b2c.png)

#### Set
Redis set对外提供的功能与list类似是一个列表的功能，特殊之处在于set是可以自动排重的，当你需要存储一个列表数据，又不希望出现重复数据时，set是一个很好的选择，并且set提供了判断某个成员是否在一个set集合内的重要接口，这个也是list所不能提供的。
Redis的Set是string类型的无序集合。它底层其实是一个value为null的hash表，所以添加，删除，查找的复杂度都是O(1)。
一个算法，随着数据的增加，执行时间的长短，如果是O(1)，数据增加，查找数据的时间不变


#### Hash
Redis hash 是一个键值对集合。
Redis hash是一个string类型的field和value的映射表，hash特别适合用于存储对象。
类似Java里面的Map<String,Object>
用户ID为查找的key，存储的value用户对象包含姓名，年龄，生日等信息，如果用普通的key/value结构来存储
主要有以下2种存储方式：
![img](https://img-blog.csdnimg.cn/a0f189254db44c5e9b5c25e91e925714.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAZG9vbXdhdGNoZXI=,size_20,color_FFFFFF,t_70,g_se,x_16)

#### Sorted Set(Zset)
Redis有序集合zset与普通集合set非常相似，是一个没有重复元素的字符串集合。
不同之处是有序集合的每个成员都关联了一个评分（score）,这个评分（score）被用来按照从最低分到最高分的方式排序集合中的成员。集合的成员是唯一的，但是评分可以是重复了 。
因为元素是有序的, 所以你也可以很快的根据评分（score）或者次序（position）来获取一个范围的元素。
访问有序集合的中间元素也是非常快的,因此你能够使用有序集合作为一个没有重复成员的智能列表


### 1.3 stream
[参考](https://blog.csdn.net/lt_xiaodou/article/details/126525965)

Redis Stream 是Redis5.0推出的一种专门用来处理消息队列场景的高级数据结构，是Redis下消息队列的最佳实现。
![img](https://img-blog.csdnimg.cn/7680e75ca6a04455af8c04f685151b9d.png)

### 1.4 windows应用
[redis 5 windows版本下载](https://github.com/tporadowski/redis/releases)

### 1.5 Reids6新特性
[Redis 6 新特性](https://blog.csdn.net/m0_67393342/article/details/123678805)

[Redis6全数据类型详解](https://blog.csdn.net/doomwatcher/article/details/120623082)

## 二、Redis应用
[Redis可视化工具-官方推荐](https://github.com/RedisInsight/RedisInsight)

### 2.1 SpringBoot整合Reids
[参考](https://blog.csdn.net/twotwo22222/article/details/127755552)
#### 2.1.1 初始化
+ 引入依赖

    ```
    <!-- redis依赖包 -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-redis</artifactId>
    </dependency>
    ```

+ 配置

    ```
    redis:
    database: 0      # 默认数据库
    host: 127.0.0.1  # 主机地址
    pool:
        max-active: 8  # 最大存活
        max-idle: 8    # 最大闲置
        max-wait: -1
        min-idle: 0
    port: 6379      # 端口
    timeout: 3000   # 超时时间
    ```
#### 2.1.2 RedisTemplate
[redisTemplate.opsForStream()操作](https://blog.csdn.net/shuaishuaideyu/article/details/120209107)

[RedisTemplate 常用方法汇总](https://blog.csdn.net/weixin_44259720/article/details/125490613)


+ 配置类RedisConfig
配置key和value系列化，为什么要配置？
因为不配置默认编码会导致key和value乱码。
```
@Configuration
@EnableCaching
public class RedisConfig extends CachingConfigurerSupport{
    @Bean(name="redisTemplate")
    public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate<String,Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        redisTemplate.setKeySerializer(keySerializer());
        redisTemplate.setHashKeySerializer(keySerializer());
        redisTemplate.setValueSerializer(valueSerializer());
        redisTemplate.setHashValueSerializer(valueSerializer());
    }
    
    private RedisSerializer<String> keySerializer(){
        return new StringRedisSerializer();
    }
    //使用jackson系列化器
    private RedisSerializer<Object> valueSerializer(){
        return new GenericJackson2JsonRedisSerializer();
    }
}
```

### 2.2 缓存场景
+ 缓存
    + 缓存状态数据字典
    
+ 更新机制

### 2.3 消息队列场景
[SpringBoot 中使用Redis Stream 实现消息监听优化](https://blog.csdn.net/hhl18730252820/article/details/122702264)

