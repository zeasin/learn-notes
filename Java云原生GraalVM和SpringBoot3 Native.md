# Java云原生GraalVM和SpringBoot3 Native
## 一、GraaleVM初探（Windows环境）
GraalVM属于JVM的一种，是OpenJDK其中的一种实现。

[参考](https://zhuanlan.zhihu.com/p/450007089)

[参考2](https://blog.csdn.net/lizhou828/article/details/124933981)

### 1.1、配置GraaleVm
+ 下载GraaleVm
[https://github.com/graalvm/graalvm-ce-builds/releases](https://github.com/graalvm/graalvm-ce-builds/releases)

+ 设置windows环境变量
  + GRAALVM_HOME
  + Path加入`%GRAALVM_HOME%\bin`

### 1.2、安装native-image
+ 在线安装
`gu install native-image`由于网络问题可能会安装失败
+ 离线安装
`gu install -L 文件下载的本地位置`
  + 离线安装包下载
  [https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-21.0.0.2/wasm-installable-svm-java11-windows-amd64-21.0.0.2.jar](https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-21.0.0.2/wasm-installable-svm-java11-windows-amd64-21.0.0.2.jar)

### 1.3、安装C++桌面编译工具
**注意我们这边安装它只是为了，让它编译Java为本地文件，所以只选择了两个功能**
#### 离线缓存Visual Studio 2022安装包
  + 下载安装包[下载地址](https://visualstudio.microsoft.com/zh-hans/downloads/)
  + 离线缓存

  CMD运行`vs_community.exe --layout d:\vs`

#### 安装MSVC、Windows 10 SDK
选择使用c++的桌面开发 可选 点 msvc v142以及windows10 sdk 选择好后点修改

![img](https://pics3.baidu.com/feed/7acb0a46f21fbe09f02522d6d1a010388544adc1.png@f_auto?token=03bf4eb7ae420a73c44fe1bd444db399)
+ MSVC v142
+ Windows 10 SDK（10.0.19041.0）


#### 配置c++编译环境

[配置参考](https://blog.csdn.net/helloexp/article/details/129088390)

  + 在PATH中添加：`C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.16.27023\bin\HostX64\x64`
  + 添加环境变量`INCLUDE`
  ```
  C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.16.27023\include
  C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\shared
  C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\um
  C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\winrt
  C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\ucrt

  ```
  + 添加环境变量`LIB`(64位系统)
  ```
  C:\Program Files (x86)\Windows Kits\10\Lib\10.0.19041.0\um\x64
  C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.16.27023\lib\x64
  C:\Program Files (x86)\Windows Kits\10\Lib\10.0.19041.0\ucrt\x64

  ```

### 1.4、从HelloWorld开始
[参考](https://juejin.cn/post/7048902171777040421)
+ Java
```
public class HelloWorld {
  public static void main(String[] args) {
    System.out.println("Hello, World!");
  }
}
```
+ javac
`javac HelloWorld.java `

+ native-image
`native-image HelloWorld`
或
`native-image -jar [jar包全路径] [编译后的文件名称]`

+ 运行helloworld.exe

## 二、Spring Native
### 2.1 SpringAOT介绍
GraalVM为了把Java程序编译为一个可执行的二进制文件，需要预先确定程序中用到的所有类，但是Java程序中很有可能某些类是动态生成的，比如很多框架中都用到了动态代理，从而程序运行过程中会动态生成一些类。

为了解决这个问题，比较笨的办法是，通过配置文件指定哪些类是动态生成的，比较聪明的办法是，先运行一下程序自动找到哪些类是动态生成的。

GraalVM这两种办法都是支持的，这样对于Spring、SpringBoot这些框架就能省事很多了，

这样，对于一个SpringBoot应用程序，就可以利用GraalVM将它编译成为一个可执行的二进制文件了。

当然，Spring及SpringBoot为了进一步提升启动速度，Spring及SpringBoot自身也做了一些优化，比如Spring AOT将Bean扫描转移到了编译期来做，从而能进一步提升启动速度。

### 2.2 SpringNative介绍
Spring Native 支持使用 GraalVM 将 Spring 的应用程序编译成本地可执行的镜像文件，可以显著提升启动速度、峰值性能以及减少内存使用。

我们传统的应用都是编译成字节码，然后通过 JVM 解释并最终编译成机器码来运行，而 Spring Native 则是通过 AOT 提前编译为机器码，在运行时直接静态编译成可执行文件，不依赖 JVM。

### 2.3 SpringBoot3 Native应用
**要注意的是spring boot3需要GraalVM 22.3以上的版本支持，大家可不要下载错了。**

[参考](https://blog.csdn.net/BASK2311/article/details/128830624)


**注意：mvn -Pnative native:compile 命令出错https://blog.csdn.net/OpenGao/article/details/128273474**

+ 构建SpirngBoot3项目
```
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>3.0.6</version>
    <relativePath/> 
</parent>
```
+ 加入native-maven-plugin插件
```
<plugin>
    <groupId>org.graalvm.buildtools</groupId>
    <artifactId>native-maven-plugin</artifactId>
</plugin>
```
这里插件会标红，我们需要引入
```
<dependency>
    <groupId>org.graalvm.buildtools</groupId>
    <artifactId>native-maven-plugin</artifactId>
    <version>0.9.21</version>
</dependency>
```
+ 加入spring-boot-maven-plugin插件
```
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
</plugin>
```
+ 尝试编译

`mvn native:build`

`mvn -Pnative native:compile`

出现错误`execution of .args returned non-zero result`

参考：

![img](https://img-blog.csdnimg.cn/010d051c8a094d198644385fa6b5fcde.png)

[参考文档](https://blog.csdn.net/OpenGao/article/details/128273474)

依然报错：


#### mvn指定jdk版本运行
以Windows为例，找到maven安装目录，编辑 `bin\mvn.cmd`

在文件开头加入 set JAVA_HOME= 路径中有空格也无须用引号
[参考](https://blog.csdn.net/whatday/article/details/128762409)





