# OCR之Tesseract-ocr搭建测试

[官方网站](https://tesseract-ocr.github.io/)


### 一、Tesseract-ocr环境配置Windows


#### 1.1 下载安装
+ 版本 5.3.x `tesseract-ocr-w64-setup-v5.3.0.20221214`
+ 下载地址：[https://digi.bib.uni-mannheim.de/tesseract/](https://digi.bib.uni-mannheim.de/tesseract/)
+ 安装

#### 1.2 配置path环境编程
+ 将安装目录配置到path环境变量


#### 1.3 测试
+ cmd `tesseract -v`


### 二、Tesseract-ocr测试
+ 识别图片测试
    进入图片文件夹：`tesseract c122.jpe result -l chi_sim`（输出到当前文件夹result.txt）
    
    如果要输出到cmd窗口

    `tesseract c122.jpe stdout -l chi_sim`
    

+ 下载中文包`chi_sim.traineddata`（下载地址：https://github.com/tesseract-ocr/tessdata）


### 三、Tesseract-ocr训练
默认中文包识别率很低，tesseract 4.0之后开始使用机器学习来进行字符识别。
这里使用`jTessBoxEditorFX`工具进行训练：

#### 1、安装jTessBoxEditorFX
+ 下载 `https://sourceforge.net/projects/vietocr/files/jTessBoxEditor/`
+ 解压
+ 运行`train.bat`


#### 2、训练步骤

[参考文档](https://www.jianshu.com/p/c290e0583629)
[参考文件2](https://blog.csdn.net/Sunday_Start/article/details/127122550)

##### 2.1 将图片合并为tiff图片(可选多张合并成一张)

格式为：[lang].[fontname].exp[num].tif
比如：yjc.font.exp0.tif
具体操作：JTessBoxEditorFX->Tools->Merge TIFF... 选择图片（可选多张）
         保存为`.tif`文件

![](https://upload-images.jianshu.io/upload_images/24252589-2d1cafccfefce0c6.png?imageMogr2/auto-orient/strip|imageView2/2/w/1180/format/webp)


##### 2.2 生成TIFF图片的box文件

打开`CMD`运行`tesseract qlp.font.exp0.tif qlp.font.exp0 batch.nochop makebox`

##### 2.3 调整box
+ 2.3.1 使用`jTessBoxEditorFx`打开`tif`文件
        ![](https://upload-images.jianshu.io/upload_images/24252589-b70d4b14d0c221cf.png?imageMogr2/auto-orient/strip|imageView2/2/w/933/format/webp)
    
+ 2.3.2 调整识别内容
        ![](https://upload-images.jianshu.io/upload_images/24252589-9f0760a8ec6757c9.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

+ 2.3.3 保存


##### 2.4 训练
    [训练步骤参考-没有成功](https://blog.csdn.net/juzicode00/article/details/121538270)
    [训练参考-成功但可能是旧版本](https://blog.csdn.net/yongshi6/article/details/50773760)


+ 2.4.1 生成lstm文件
    
    从当前工作目录下的训练用traineddata文件中抽取lstm文件，这里用当前目录下的eng.traineddata文件生成eng.lstm文件

    
    `注意：需要使用tessdata_best语言包`


    `combine_tessdata -e chi_sim.traineddata chi.lstm`

+ 2.4.2 生成lstmf文件

    这一步利用tif图片文件生成lstmf文件，如下命令会生成arabnum.mnist.exp0.lstmf文件，这个是典型tesseract命令：

    `tesseract qlp.font.exp0.tif  qlp.font.exp0  -l chi_sim  --psm 13  lstm.train`

+ 2.4.3 生成lstmf清单文件

    手动新建一个`qlp.font.exp0.list.txt`文本文件，输入上一步骤生成的lstmf文件的路径和文件名，
    
    输入qlp.font.exp0完整路径“arabnum.mnist.exp0.lstmf”


+ 2.4.4 开始训练
    `lstmtraining  --debug_interval -5  --max_iterations 9000  --target_error_rate 0.01  --continue_from=out_mnist\eng.lstm  --model_output=out_mnist\mod_out  --train_listfile=out_mnist\arabnum.mnist.exp0.list.txt  --traineddata=eng.traineddata  `

##### 2.5 生成traineddata文件
`lstmtraining  --stop_training  --traineddata=eng.traineddata  --continue_from=out_mnist\mod_out_checkpoint  --model_output=mnist.traineddata `