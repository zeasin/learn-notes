# Spring框架学习

### 一、IOC
IOC是SpringFramework核心，简单理解就是bean容器，也就是说代码中不需要new()来创建实例了，IOC启动的时候就帮你装载好了，你只需要@Autowired引入即可使用。
[Spring Bean生命周期](https://blog.csdn.net/qq_38826019/article/details/117389466)

#### 1.1 Spring Bean生命周期
![img](https://img-blog.csdnimg.cn/20210529213228111.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4ODI2MDE5,size_16,color_FFFFFF,t_70)

#### 1.2 Spring IOC扩展点
![img](https://img-blog.csdnimg.cn/20210529215324841.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4ODI2MDE5,size_16,color_FFFFFF,t_70)

### 二、AOP
AOP（Aspect Oriented Programming) 即面向切面编程，是OOP(Object Oriented Programming、面向对象编程)的一个延续，是OOP的一个进阶。

大白话就是：

将一些通用的逻辑集中实现，然后通过 AOP 进行逻辑的切入，减少了零散的碎片化代码，提高了系统的可维护性。

具体是含义可以理解为：通过代理的方式，在调用想要的对象方法时候，进行拦截处理，执行切入的逻辑，然后再调用真正的方法实现。



#### 2.1 基本概念
**关于Spring AOP的一些术语**
+ 切面（Aspect）：在Spring AOP中，切面可以使用通用类或者在普通类中以@Aspect 注解（@AspectJ风格）来实现

+ 连接点（Joinpoint）：在Spring AOP中一个连接点代表一个方法的执行

+ 通知（Advice）：在切面的某个特定的连接点（Joinpoint）上执行的动作。通知有各种类型，其中包括"around"、"before”和"after"等通知。许多AOP框架，包括Spring，都是以拦截器做通知模型， 并维护一个以连接点为中心的拦截器链

+ 切入点（Pointcut）：定义出一个或一组方法，当执行这些方法时可产生通知，Spring缺省使用AspectJ切入点语法。

**通知类型**

+ 前置通知（@Before）：在某连接点（join point）之前执行的通知，但这个通知不能阻止连接点前的执行（除非它抛出一个异常）

+ 返回后通知（@AfterReturning）：在某连接点（join point）正常完成后执行的通知：例如，一个方法没有抛出任何异常，正常返回

+ 抛出异常后通知（@AfterThrowing）：方法抛出异常退出时执行的通知

+ 后通知（@After）：当某连接点退出的时候执行的通知（不论是正常返回还是异常退出）

+ 环绕通知（@Around）：包围一个连接点（join point）的通知，如方法调用。这是最强大的一种通知类型，环绕通知可以在方法调用前后完成自定义的行为，它也会选择是否继续执行连接点或直接返回它们自己的返回值或抛出异常来结束执行。

#### 2.2 使用场景

+ 权限管理
    + 方案详述：在@ControllerAdvice里边，处理全局请求，控制权限。
    + 权限管理的其他方案：（除了AOP之外的方案）
        + 在过滤器或者拦截器中处理
        + 使用Shiro中间件

+ 异常处理
    + 情景1：在@ControllerAdvice里边，处理全局异常
    + 情景2：将Dubbo接口作为切面，统一处理Dubbo接口里边的异常

+ 操作日志
    + 情景1：按产品的需求，有的接口需要记录操作日志
        + 自定义注解，需要记录操作日志的，则在Controller的方法上加此注解
        + AOP中判断，如果有这个自定义注解，则将参数异步写到日志数据库

+ 将数据同步到ES
    + 情景1：增删改数据时，同时要处理MySQL和ES
     + 将相关类作为切面，若数据库提交，则写到ES；若回滚，则不写到ES

+ 事务控制
    + 情景1：使用Spring的@Transactional


#### 2.3 日志实例展示

##### 2.3.1、新建注解类
```
package com.ywj.log;
 
import java.lang.annotation.*;

@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SystemCrmlog {
    /**
     * 日志描述
     * 进行了什么操作
     */
    String value()  default "";
 

```
##### 2.3.2、定义切面类
```
@Slf4j
@Aspect
@Component
public class SystemLogAspect {
 
 
    /**
     * 注解Pointcut切入点
     * 定义出一个或一组方法，当执行这些方法时可产生通知
     * 指向你的切面类方法
     * 由于这里使用了自定义注解所以指向你的自定义注解
     */
    @Pointcut("@annotation(com.ywj.log.SystemCrmlog)")
    public void log() {
    }

    @Before(value = "log()")
    public  void doBefore(JoinPoint joinPoint ){

        log.info("｛｝::{}执行前。。。。。参数：｛｝。。。。。。。。。。",
            joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName(),
            Arrays.toString(joinPoint.getArgs())
        );
    }

}
```

##### 2.3.3、注解使用
代码中`@SystemCrmlog`注解即可

